package PCS.Classes;

import java.time.LocalDateTime;

public class Ticket {
    private int id;
    private LocalDateTime enteringTime;
    private LocalDateTime payTime;
    private double fee;
    private int payMachineld;
    private boolean isPay;

    public Ticket(){
        LocalDateTime now = LocalDateTime.now();
        this.enteringTime = now;
        this.payTime = null;
        this.fee = 0;
        this.payMachineld = 0;
        this.isPay = false;
    }

    public boolean isPay(){
        return isPay;
    }

    public boolean isExceedTime(int stayTime){
        LocalDateTime allowLevelTime = payTime.plusMinutes(stayTime);
        LocalDateTime now = LocalDateTime.now();
        return !now.isBefore(allowLevelTime);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getEnteringTime() {
        return enteringTime;
    }

    public void setEnteringTime(LocalDateTime enteringTime) {
        this.enteringTime = enteringTime;
    }

    public LocalDateTime getPayTime() {
        return payTime;
    }

    public void setPayTime(LocalDateTime payTime) {
        this.payTime = payTime;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public int getPayMachineld() {
        return payMachineld;
    }

    public void setPayMachineld(int payMachineld) {
        this.payMachineld = payMachineld;
    }

    public void setPay(boolean pay) {
        isPay = pay;
    }
}
