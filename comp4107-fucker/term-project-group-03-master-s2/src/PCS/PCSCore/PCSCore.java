package PCS.PCSCore;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.*;
import AppKickstarter.timer.Timer;

import PCS.Classes.Ticket;

import java.util.Hashtable;


//======================================================================
// PCSCore
public class PCSCore extends AppThread {
    private MBox gateMBox;
    private MBox collectorMBox;
    private MBox dispatcherMBox;
    private final int pollTime;
    private final int PollTimerID=1;
    private final int openCloseGateTime;		// for demo only!!!
    private final int OpenCloseGateTimerID=2;		// for demo only!!!
    private boolean gateIsClosed = true;		// for demo only!!!
    private final int StayMinutes;
	private Hashtable<Integer, Ticket> Tickets;


    //------------------------------------------------------------
    // PCSCore
    public PCSCore(String id, AppKickstarter appKickstarter) {
        super(id, appKickstarter);
        this.pollTime = Integer.parseInt(appKickstarter.getProperty("PCSCore.PollTime"));
        this.openCloseGateTime = Integer.parseInt(appKickstarter.getProperty("PCSCore.OpenCloseGateTime"));        // for demo only!!!
        this.StayMinutes = Integer.parseInt(appKickstarter.getProperty("PCSCore.StayMinutes"));
        this.Tickets = new Hashtable<Integer, Ticket>();
    } // PCSCore


    //------------------------------------------------------------
    // run
    public void run() {
        Thread.currentThread().setName(id);
        Timer.setTimer(id, mbox, pollTime, PollTimerID);
        Timer.setTimer(id, mbox, openCloseGateTime, OpenCloseGateTimerID);    // for demo only!!!
        appendTextArea(id + ": starting...", "Info");
        log.info(id + ": starting...");

        gateMBox = appKickstarter.getThread("GateHandler").getMBox();
        collectorMBox = appKickstarter.getThread("CollectorHandler").getMBox();
        dispatcherMBox = appKickstarter.getThread("DispatcherHandler").getMBox();

        for (boolean quit = false; !quit; ) {
            Msg msg = mbox.receive();

            appendTextArea(id + ": message received: [" + msg + "].", "Fine");
            log.fine(id + ": message received: [" + msg + "].");

            switch (msg.getType()) {
                case TimesUp:
                    handleTimesUp(msg);
                    break;

                case GateOpenReply:
                    appendTextArea(id + ": Gate is opened.", "Info");
                    log.info(id + ": Gate is opened.");
                    gateIsClosed = false;
                    break;

                case GateCloseReply:
                    appendTextArea(id + ": Gate is closed.", "Info");
                    log.info(id + ": Gate is closed.");
                    gateIsClosed = true;
                    break;

                case PollAck:
                    appendTextArea("PollAck: " + msg.getDetails(), "Info");
                    log.info("PollAck: " + msg.getDetails());
                    break;

                case ValidatingTicket:
                    Ticket insertedTicket = Tickets.get(Integer.parseInt(msg.getDetails()));
                    if(insertedTicket != null) {
                        appendTextArea(id + ": Receive inserted ticket id : " + msg.getDetails() + " and validating now.", "Info");
                        log.info(id + ": Receive inserted ticket id : " + msg.getDetails() + " and validating now.");
                        if (insertedTicket.isPay() && !insertedTicket.isExceedTime(StayMinutes)) {
                            //Payed
                            appendTextArea(id + ": Ticket validating success.", "Info");
                            appendTextArea(id + ": Sending positive acknowledgement to collector and open gate signal.", "info");
                            collectorMBox.send(new Msg(id, mbox, Msg.Type.PositiveAcknowledgement, "Ticket validation is successful"));
                            gateMBox.send(new Msg(id, mbox, Msg.Type.GateOpenRequest, "Ticket validation is successful and open gate"));
                        } else {
                            appendTextArea(id + ": Ticket validating unsuccess.", "Warning");
                            appendTextArea(id + ": Sending negative acknowledgement to .", "Warning");
                            String reason = "";
                            //Not Yet Payed
                            if (!insertedTicket.isPay()) {
                                reason += "Not yet pay ";
                            } else {
                                //Exceed Stay Time
                                if (insertedTicket.isExceedTime(StayMinutes)) {
                                    reason += "Exceed Stay time.";
                                }
                            }
                            collectorMBox.send(new Msg(id, mbox, Msg.Type.NegativeAcknowledgement, reason));
                        }
                    }else{
                        appendTextArea(id + ": Receive inserted ticket id : " + msg.getDetails() + ",but pcs not find ticket record.", "Warning");
                        log.info(id + ": Receive inserted ticket id : " + msg.getDetails() + ",but pcs not find ticket record.");
                        collectorMBox.send(new Msg(id, mbox, Msg.Type.IncorrectTicketId, msg.getDetails()));
                    }

                    break;

                case TicketPrintReply:
                    Ticket Ticket = new Ticket();
                    Ticket.setId(Tickets.size()+1);
                    Tickets.put((Ticket.getId()),Ticket);
                    appendTextArea(id + ": Ticket No. " + Ticket.getId() + " printed successfully", "Info");
                    log.info(id + ": Ticket No. " + Ticket.getId() + " printed successfully");
                    //dispatcherMBox.send(new Msg(id, mbox, Msg.Type.TicketPrintReply, "Ticket printed successfully"));
                    break;

                case TicketDispatchReply:
                    appendTextArea(id + ": Ticket dispatched successfully", "Info");
                    log.info(id + ": Ticket dispatched successfully");
                    gateMBox.send(new Msg(id, mbox, Msg.Type.GateOpenRequest,"Ticket dispatched successfully, request gate open"));
                    //dispatcherMBox.send(new Msg(id, mbox, Msg.Type.TicketDispatchReply, "Ticket dispatched successfully"));
                    break;

                case Terminate:
                    quit = true;
                    break;

                default:
                    log.warning(id + ": unknown message type: [" + msg + "]");
            }
        }

        // declaring our departure
        appKickstarter.unregThread(this);
        log.info(id + ": terminating...");
    } // run


    //------------------------------------------------------------
    // run
    private void handleTimesUp(Msg msg) {
        log.info("------------------------------------------------------------");
        switch (Timer.getTimesUpMsgTimerId(msg)) {
            case PollTimerID:
                appendTextArea("Poll: " + msg.getDetails(), "Info");
                log.info("Poll: " + msg.getDetails());
                gateMBox.send(new Msg(id, mbox, Msg.Type.Poll, ""));
                Timer.setTimer(id, mbox, pollTime, PollTimerID);
                break;

            case OpenCloseGateTimerID:                    // for demo only!!!
                if (gateIsClosed) {
                    appendTextArea(id + ": Open the gate now (for demo only!!!)", "Info");
                    log.info(id + ": Open the gate now (for demo only!!!)");
                    gateMBox.send(new Msg(id, mbox, Msg.Type.GateOpenRequest, ""));
                } else {
                    appendTextArea(id + ": Close the gate now (for demo only!!!)", "Info");
                    log.info(id + ": Close the gate now (for demo only!!!)");
                    gateMBox.send(new Msg(id, mbox, Msg.Type.GateCloseRequest, ""));
                }
                Timer.setTimer(id, mbox, openCloseGateTime, OpenCloseGateTimerID);
                break;

            default:
                appendTextArea(id + ": why am I receiving a timeout with timer id " + Timer.getTimesUpMsgTimerId(msg), "Severe");
                log.severe(id + ": why am I receiving a timeout with timer id " + Timer.getTimesUpMsgTimerId(msg));
                break;
        }

    } // handleTimesUp

    //------------------------------------------------------------
    // displayMessage
    protected void appendTextArea(String Message,String Type) {
        log.info(Message);
    } // displayMessage

} // PCSCore
