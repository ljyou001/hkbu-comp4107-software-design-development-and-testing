package PCS.CollectorHandler.Emulator;

import AppKickstarter.misc.*;
import AppKickstarter.timer.Timer;

import PCS.PCSStarter;
import PCS.CollectorHandler.CollectorHandler;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import java.util.concurrent.TimeUnit;

//======================================================================
// CollectorEmulator
public class CollectorEmulator extends CollectorHandler {
    private Stage myStage;
    private CollectorEmulatorController CollectorEmulatorController;
    private final PCSStarter pcsStarter;
    private final String id;
    private boolean autoPoll;


    //------------------------------------------------------------
    // CollectorEmulator
    public CollectorEmulator(String id, PCSStarter pcsStarter) {
        super(id, pcsStarter);
        this.pcsStarter = pcsStarter;
        this.id = id + "Emulator";
        this.autoPoll = true;
    } // CollectorEmulator


    //------------------------------------------------------------
    // start
    public void start() throws Exception {
        Parent root;
        myStage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        String fxmlName = "CollectorEmulator.fxml";
        loader.setLocation(CollectorEmulator.class.getResource(fxmlName));
        root = loader.load();
        CollectorEmulatorController = (CollectorEmulatorController) loader.getController();
        CollectorEmulatorController.initialize(id, pcsStarter, log, this);
        myStage.initStyle(StageStyle.DECORATED);
        myStage.setScene(new Scene(root, 420, 470));
        myStage.setTitle("Collector Emulator");
        myStage.setResizable(false);
        myStage.setOnCloseRequest((WindowEvent event) -> {
            pcsStarter.stopApp();
            Platform.exit();
        });
        myStage.show();
    } // CollectorEmulator


    //------------------------------------------------------------
    // processMsg
    protected final boolean processMsg(Msg msg) {
        boolean quit = false;

        switch (msg.getType()) {
            case CollectorEmulatorAutoPollToggle:
                handleCollectorEmulatorAutoPollToggle();
                break;
            default:
                quit = super.processMsg(msg);
        }
        return quit;
    } // processMsg


    //------------------------------------------------------------
    // handleValidatingTicket
    @Override
    protected void sendValidatingTicketSuccess() {
        logFine("Validating ticket success and sended open gate signal to pcs.");
    } // handleValidatingTicket

    //------------------------------------------------------------
    // displayIncorrectTicketId
    @Override
    protected void displayIncorrectTicketId(String TicketId) {
        logWarning("Ticket Id incorrect.");
    } // displayIncorrectTicketId

    //------------------------------------------------------------
    // handleValidatingTicket
    @Override
    protected void sendValidatingTicketUnSuccess(String Reason) {
        logWarning("Validating ticket unsuccessful.");
        logWarning("Reason : " + Reason);
    } // handleValidatingTicket

    //------------------------------------------------------------
    // handleValidatingTicket
    @Override
    protected void sendRingAlarmSignal(boolean RingAlarm) {
            if(RingAlarm){
                try {
                    logWarning("Alarm : ring ring ring !!!");
                    TimeUnit.SECONDS.sleep(2);
                    mbox.send(new Msg(id, mbox, Msg.Type.KeepRingingAlarm, "Keep Ringing Alarm"));
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
    } // handleValidatingTicket

    //------------------------------------------------------------
    // handleValidatingTicket
    @Override
    protected void sendManualOverrideSignal() {
        logFine("Manual Override.");
    } // handleValidatingTicket

    //------------------------------------------------------------
    // sendPollReq
    @Override
    protected void sendPollReq() {
        logFine("Poll request received.  [autoPoll is " + (autoPoll ? "on]" : "off]"));
        if (autoPoll) {
            logFine("Send poll ack.");
            mbox.send(new Msg(id, mbox, Msg.Type.PollAck, ""));
        }
    } // sendPollReq

    //------------------------------------------------------------
    // handleCollectorEmulatorAutoPollToggle:
    public final boolean handleCollectorEmulatorAutoPollToggle() {
        autoPoll = !autoPoll;
        logInfo("Auto poll change: " + (autoPoll ? "off --> on" : "on --> off"));
        return autoPoll;
    } // handleCollectorEmulatorAutoPollToggle

    //------------------------------------------------------------
    // logFine
    private final void logFine(String logMsg) {
        CollectorEmulatorController.appendTextArea("[FINE]: " + logMsg);
        log.fine(id + ": " + logMsg);
    } // logFine


    //------------------------------------------------------------
    // logInfo
    private final void logInfo(String logMsg) {
        CollectorEmulatorController.appendTextArea("[INFO]: " + logMsg);
        log.info(id + ": " + logMsg);
    } // logInfo


    //------------------------------------------------------------
    // logWarning
    private final void logWarning(String logMsg) {
        CollectorEmulatorController.appendTextArea("[WARNING]: " + logMsg);
        log.warning(id + ": " + logMsg);
    } // logWarning


    //------------------------------------------------------------
    // logSevere
    private final void logSevere(String logMsg) {
        CollectorEmulatorController.appendTextArea("[SEVERE]: " + logMsg);
        log.severe(id + ": " + logMsg);
    } // logSevere
} // CollectorEmulator
