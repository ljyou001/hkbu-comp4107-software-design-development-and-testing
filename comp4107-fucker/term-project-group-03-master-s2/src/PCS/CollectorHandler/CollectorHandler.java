package PCS.CollectorHandler;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.*;

/*****************************************
 * -----------------hardware
 * manual override button
 * -----------------function
 * vail/read ticket
 * ring alarm
 * stop ring alarm
 * send open gate signal to PCS ///////////////PCS send signal to for close gate
 * send a positive acknowledgement
 * send a negative acknowledgement
 * enable manual override / manual override reply
 */

//======================================================================
// CollectorHandler
public class CollectorHandler extends AppThread {
    protected final MBox pcsCore;
    private CollectorStatus collectorStatus;
    private boolean keepRingAlarm;

    //------------------------------------------------------------
    // CollectorHandler
    public CollectorHandler(String id, AppKickstarter appKickstarter) {
        super(id, appKickstarter);
        pcsCore = appKickstarter.getThread("PCSCore").getMBox();
        collectorStatus = CollectorStatus.Idle;
    } // CollectorHandler


    //------------------------------------------------------------
    // run
    public void run() {
        Thread.currentThread().setName(id);
        log.info(id + ": starting...");

        for (boolean quit = false; !quit; ) {
            Msg msg = mbox.receive();

            log.fine(id + ": message received: [" + msg + "].");

            quit = processMsg(msg);
        }

        // declaring our departure
        appKickstarter.unregThread(this);
        log.info(id + ": terminating...");
    } // run


    //------------------------------------------------------------
    // processMsg
    protected boolean processMsg(Msg msg) {
        boolean quit = false;

        switch (msg.getType()) {
            case ValidatingTicket: handleValidatingTicket(msg.getDetails()); break;
            case IncorrectTicketId: handleIncorrectTicketId(msg.getDetails()); break;
            case PositiveAcknowledgement: handleValidatingTicketSuccess(); break;
            case NegativeAcknowledgement: handleValidatingTicketUnsuccessful(msg.getDetails()); break;
            case manualOverride: handleManualOverrideRequest(); break;
            case KeepRingingAlarm: sendRingAlarmSignal(keepRingAlarm);; break;
            case Poll: handlePollReq(); break;
            case PollAck: handlePollAck(); break;
            case Terminate: quit = true; break;
            default: log.warning(id + ": unknown message type: [" + msg + "]");
        }
        return quit;
    } // processMsg

    //------------------------------------------------------------
    // handleValidatingTicket
    protected void handleValidatingTicket(String ticketId){
        log.info(id + ": Collector receive ticket.");

        CollectorStatus oldCollectorStatus = collectorStatus;
        switch (collectorStatus) {
            case Idle:
                log.info(id + ": Sending ticket information to pcs for valid.");
                pcsCore.send(new Msg(id, mbox, Msg.Type.ValidatingTicket, ticketId));
                collectorStatus = CollectorStatus.ValidatingTicket;
                break;

            case ValidatingTicket:
                log.warning(id + ": Collector validating ticket now, please wait!! Ignore request.");
                break;

            case RingingAlarm:
                log.warning(id + ": Collector ringing alarm!! Ignore request.");
                break;

            default:
                log.warning(id + ": unknown status type: [" + collectorStatus + "]");
        }

        if (oldCollectorStatus != collectorStatus) {
            log.fine(id + ": Collector status change: " + oldCollectorStatus + " --> " + collectorStatus);
        }
    } // handleValidatingTicket

    //------------------------------------------------------------
    // handleIncorrectTicketId
    protected void handleIncorrectTicketId(String TicketId){
        log.info(id + ": Collector receive not find Ticket with " + TicketId +" .");

        CollectorStatus oldCollectorStatus = collectorStatus;
        switch (collectorStatus) {
            case Idle:
                log.info(id + ": Collector idle now.");
                break;

            case ValidatingTicket:
                log.warning(id + ": Collector validating, but not find ticket with " + TicketId);
                displayIncorrectTicketId(TicketId);
                collectorStatus = CollectorStatus.Idle;
                break;

            case RingingAlarm:
                log.warning(id + ": Collector ringing alarm!! Ignore request.");
                break;

            default:
                log.warning(id + ": unknown status type: [" + collectorStatus + "]");
        }

        if (oldCollectorStatus != collectorStatus) {
            log.fine(id + ": Collector status change: " + oldCollectorStatus + " --> " + collectorStatus);
        }
    } // handleIncorrectTicketId

    //------------------------------------------------------------
    // handleValidatingTicket
    protected void handleValidatingTicketSuccess(){
        log.info(id + ": Collector receive valid ticket success form PCS.");

        CollectorStatus oldCollectorStatus = collectorStatus;
        switch (collectorStatus) {
            case Idle:
                log.warning(id + ": Collector is idle now.");
                break;

            case ValidatingTicket:
                log.info(id + ": Collector validating ticket success.");
                sendValidatingTicketSuccess();
                collectorStatus = CollectorStatus.Idle;
                break;

            case RingingAlarm:
                log.warning(id + ": Collector is ringing alarm now!!");
                break;

            default:
                log.warning(id + ": unknown status type: [" + collectorStatus + "]");
        }

        if (oldCollectorStatus != collectorStatus) {
            log.fine(id + ": Collector status change: " + oldCollectorStatus + " --> " + collectorStatus);
        }
    } // handleValidatingTicket

    //------------------------------------------------------------
    // handleValidatingTicketUnsuccessful
    protected void handleValidatingTicketUnsuccessful(String reason){
        log.info(id + ": Collector receive valid ticket not success form PCS.");
        log.warning(id + ": " + reason);

        CollectorStatus oldCollectorStatus = collectorStatus;
        switch (collectorStatus) {
            case Idle:
                log.warning(id + ": Collector is idle now.");
                break;

            case ValidatingTicket:
                log.info(id + ": Collector validating ticket not success.");
                sendValidatingTicketUnSuccess(reason);
                keepRingAlarm = true;
                sendRingAlarmSignal(keepRingAlarm);
                collectorStatus = CollectorStatus.RingingAlarm;
                break;

            case RingingAlarm:
                log.warning(id + ": Collector is ringing alarm now!!");
                break;

            default:
                log.warning(id + ": unknown status type: [" + collectorStatus + "]");
        }

        if (oldCollectorStatus != collectorStatus) {
            log.fine(id + ": Collector status change: " + oldCollectorStatus + " --> " + collectorStatus);
        }
    } // handleValidatingTicketUnsuccessful

    //------------------------------------------------------------
    // handleManualOverride
    protected void handleManualOverrideRequest(){
        log.info(id + ": Collector receive manual override request");

        CollectorStatus oldCollectorStatus = collectorStatus;
        switch (collectorStatus) {
            case Idle:
                log.info(id + ": Collector is idle now. Ignore request!!");
                break;

            case ValidatingTicket:
                log.warning(id + ": Collector validating ticket now!! Ignore request.");
                break;

            case RingingAlarm:
                log.warning(id + ": Collector is stop ringing alarm now!!");
                sendManualOverrideSignal();
                keepRingAlarm = false;
                sendRingAlarmSignal(keepRingAlarm);
                collectorStatus = CollectorStatus.Idle;
                break;

            default:
                log.warning(id + ": unknown status type: [" + collectorStatus + "]");
        }

        if (oldCollectorStatus != collectorStatus) {
            log.fine(id + ": Collector status change: " + oldCollectorStatus + " --> " + collectorStatus);
        }

    } // handleManualOverride

    //------------------------------------------------------------
    // handlePollReq
    protected final void handlePollReq() {
        log.info(id + ": poll request received.  Send poll request to hardware.");
        sendPollReq();
    } // handlePollReq


    //------------------------------------------------------------
    // handlePollAck
    protected final void handlePollAck() {
        log.info(id + ": poll ack received.  Send poll ack to PCS Core.");
        pcsCore.send(new Msg(id, mbox, Msg.Type.PollAck, id + " is up!"));
    } // handlePollAck

    //------------------------------------------------------------
    protected void sendValidatingTicketSuccess(){
        // fixme: send ticket information to pcs for valid
        log.info(id + ": sending valid ticket success to hardware.");
    }//sendValidatingTicketSuccess

    //------------------------------------------------------------
    protected void displayIncorrectTicketId(String TicketId){
        // fixme: send ticket information to pcs for valid
        log.info(id + ": sending incorrect ticket id message to hardware.");
    }//sendValidatingTicketSuccess


    //------------------------------------------------------------
    protected void sendValidatingTicketUnSuccess(String Reason){
        // fixme: send ticket information to pcs for valid
        log.info(id + ": sending valid ticket unsuccess to hardware.");
    }//sendValidatingTicketUnSuccess

    //------------------------------------------------------------
    // sendRingAlarmSignal
    protected void sendRingAlarmSignal(boolean RingAlram) {
        // fixme: send gate open signal to hardware
        if(RingAlram){
            log.info(id + ": sending ring alarm signal to hardware.");
        }else{
            log.info(id + ": sending stop ring alarm signal to hardware.");
        }
    } // sendRingAlarmSignal

    //------------------------------------------------------------
    // sendManualOverrideSignal
    protected void sendManualOverrideSignal() {
        // fixme: send gate close signal to hardware
        log.info(id + ": sending manual override signal and stop ring alarm to hardware.");
    } // sendManualOverrideSignal


    //------------------------------------------------------------
    // sendPollReq
    protected void sendPollReq() {
        // fixme: send collector poll request to hardware
        log.info(id + ": poll request received");
    } // sendPollReq


    //------------------------------------------------------------
    // Collector Status
    private enum CollectorStatus {
        Idle,
        ValidatingTicket,
        RingingAlarm,
    }
} // CollectorHandler
