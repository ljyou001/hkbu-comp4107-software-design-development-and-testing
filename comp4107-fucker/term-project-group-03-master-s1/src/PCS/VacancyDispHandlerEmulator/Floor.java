package PCS.VacancyDispHandlerEmulator;

public class Floor {
	private int floorId;
	private String floorName;
	private int vacancy;
	
	public int addVacancy() { 
		vacancy = vacancy + 1;
		return vacancy;
	}
	
	public int deleteVacancy() { 
		vacancy = vacancy - 1;
		return vacancy;
	}

	public String getFloorName() {
		return floorName;
	}

	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}
	
}
