package PCS.CollectorHandler.Emulator;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.MBox;
import AppKickstarter.misc.Msg;

import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;


//======================================================================
// CollectorEmulatorController
public class CollectorEmulatorController {
    private String id;
    private AppKickstarter appKickstarter;
    private Logger log;
    private CollectorEmulator collectorEmulator;
    private MBox collectorMBox;
    public TextArea ticketId;
    public TextArea collectorTextArea;
    public Button autoPollButton;
    private int lineNo = 0;


    //------------------------------------------------------------
    // initialize
    public void initialize(String id, AppKickstarter appKickstarter, Logger log, CollectorEmulator collectorEmulator) {
        this.id = id;
        this.appKickstarter = appKickstarter;
        this.log = log;
        this.collectorEmulator = collectorEmulator;
        this.collectorMBox = appKickstarter.getThread("CollectorHandler").getMBox();
    } // initialize


    //------------------------------------------------------------
    // buttonPressed
    public void buttonPressed(ActionEvent actionEvent) {
        Button btn = (Button) actionEvent.getSource();

        switch (btn.getText()) {
            case "Insert Ticket":
                if (ticketId.getText().length() != 0) {
                    appendTextArea("Reading Ticket : " + ticketId.getText());
                    collectorMBox.send(new Msg(id, null, Msg.Type.ValidatingTicket, ticketId.getText()));
                }else{
                    appendTextArea("Please insert your ticket id.");
                }
                break;
            case "Manual Override":
                collectorMBox.send(new Msg(id, null, Msg.Type.manualOverride, "Stop Alarm"));
                appendTextArea("Manual override button pressed");
                break;

            case "Poll Request":
                appendTextArea("Send poll request.");
                collectorMBox.send(new Msg(id, null, Msg.Type.Poll, ""));
                break;

            case "Poll ACK":
                appendTextArea("Send poll ack.");
                collectorMBox.send(new Msg(id, null, Msg.Type.PollAck, ""));
                break;

            case "Auto Poll: On":
                Platform.runLater(() -> autoPollButton.setText("Auto Poll: Off"));
                collectorMBox.send(new Msg(id, null, Msg.Type.CollectorEmulatorAutoPollToggle, "ToggleAutoPoll"));
                break;

            case "Auto Poll: Off":
                Platform.runLater(() -> autoPollButton.setText("Auto Poll: On"));
                collectorMBox.send(new Msg(id, null, Msg.Type.CollectorEmulatorAutoPollToggle, "ToggleAutoPoll"));
                break;

            default:
                log.warning(id + ": unknown button: [" + btn.getText() + "]");
                break;
        }
    } // buttonPressed


    //------------------------------------------------------------
    // appendTextArea
    public void appendTextArea(String status) {
        Platform.runLater(() -> collectorTextArea.appendText(String.format("[%04d] %s\n", ++lineNo, status)));
    } // appendTextArea
} // CollectorEmulatorController
