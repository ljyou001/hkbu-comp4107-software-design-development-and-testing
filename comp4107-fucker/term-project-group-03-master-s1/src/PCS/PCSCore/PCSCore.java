package PCS.PCSCore;

import PCS.Classes.*;
import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.*;
import AppKickstarter.timer.Timer;
import java.util.*;
import java.util.logging.Logger;

import PCS.GateHandler.Emulator.GateEmulator;
import PCS.GateHandler.Emulator.GateEmulatorController;
import PCS.PCSCore.Emulator.PCSCoreEmulator;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import PCS.PCSStarter;

//======================================================================
// PCSCore
public class PCSCore extends AppThread {
    private Stage myStage;
    private MBox gateMBox;
    private MBox collectorMBox;
    private final int pollTime;
    private final int PollTimerID = 1;
    private final int openCloseGateTime;        // for demo only!!!
    private final int OpenCloseGateTimerID = 2;        // for demo only!!!
    private boolean gateIsClosed = true;        // for demo only!!!
    private final int StayMinutes;
    private Hashtable<Integer, Ticket> Tickets;


    //------------------------------------------------------------
    // PCSCore
    public PCSCore(String id, AppKickstarter appKickstarter){
        super(id, appKickstarter);
        this.pollTime = Integer.parseInt(appKickstarter.getProperty("PCSCore.PollTime"));
        this.openCloseGateTime = Integer.parseInt(appKickstarter.getProperty("PCSCore.OpenCloseGateTime"));        // for demo only!!!
        this.StayMinutes = Integer.parseInt(appKickstarter.getProperty("PCSCore.StayMinutes"));
        this.Tickets = new Hashtable<Integer, Ticket>();
    } // PCSCore

    //------------------------------------------------------------
    // run
    public void run()  {
        Thread.currentThread().setName(id);
        Timer.setTimer(id, mbox, pollTime, PollTimerID);
        Timer.setTimer(id, mbox, openCloseGateTime, OpenCloseGateTimerID);    // for demo only!!!
        log.info(id + ": starting...");

        gateMBox = appKickstarter.getThread("GateHandler").getMBox();
        collectorMBox = appKickstarter.getThread("CollectorHandler").getMBox();

        /*
        Parent root;
        myStage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        String fxmlName = "PCSCore.fxml";
        loader.setLocation(PCSCore.class.getResource(fxmlName));
        root = loader.load();
        //PCSCore = (GateEmulatorController) loader.getController();
        //PCSCore.initialize(id, pcsStarter, log, this);
        myStage.initStyle(StageStyle.DECORATED);
        myStage.setScene(new Scene(root, 420, 470));
        myStage.setTitle("PCS Core");
        myStage.setResizable(false);
        myStage.setOnCloseRequest((WindowEvent event) -> {
            pcsStarter.stopApp();
            Platform.exit();
        });
        myStage.show();
         */

        for (boolean quit = false; !quit; ) {
            Msg msg = mbox.receive();

            appendTextArea(id + ": message received: [" + msg + "].", "Fine");
            //log.fine(id + ": message received: [" + msg + "].");

            switch (msg.getType()) {
                case TimesUp:
                    handleTimesUp(msg);
                    break;

                case GateOpenReply:
                    log.info(id + ": Gate is opened.");
                    gateIsClosed = false;
                    break;

                case GateCloseReply:
                    log.info(id + ": Gate is closed.");
                    gateIsClosed = true;
                    break;

                case PollAck:
                    log.info("PollAck: " + msg.getDetails());
                    break;

                case ValidatingTicket:
                    Ticket insertedTicket = Tickets.get(Integer.parseInt(msg.getDetails()));
                    if(insertedTicket != null) {
                        //appendTextArea(id + ": Receive inserted ticket id : " + msg.getDetails() + " and validating now.");
                        //log.info(id + ": Receive inserted ticket id : " + msg.getDetails() + " and validating now.");
                        appendTextArea(id + ": Receive inserted ticket id : " + msg.getDetails() + " and validating now.", "Info");
                        if (insertedTicket.isPay() && !insertedTicket.isExceedTime(StayMinutes)) {
                            //Payed
                            appendTextArea(id + ": Ticket validating success.", "Info");
                            appendTextArea(id + ": Sending positive acknowledgement to collector and open gate signal.", "info");
                            collectorMBox.send(new Msg(id, mbox, Msg.Type.PositiveAcknowledgement, "Ticket validation is successful"));
                            gateMBox.send(new Msg(id, mbox, Msg.Type.GateOpenRequest, "Ticket validation is successful and open gate"));
                        } else {
                            appendTextArea(id + ": Ticket validating unsuccess.", "Warning");
                            appendTextArea(id + ": Sending negative acknowledgement to .", "Warning");
                            String reason = "";
                            //Not Yet Payed
                            if (!insertedTicket.isPay()) {
                                reason += "Not yet pay ";
                            } else {
                                //Exceed Stay Time
                                if (insertedTicket.isExceedTime(StayMinutes)) {
                                    reason += "Exceed Stay time.";
                                }
                            }
                            collectorMBox.send(new Msg(id, mbox, Msg.Type.NegativeAcknowledgement, reason));
                        }
                    }else{
                        appendTextArea(id + ": Receive inserted ticket id : " + msg.getDetails() + ",but pcs not find ticket record.", "Warning");
                        //log.info(id + ": Receive inserted ticket id : " + msg.getDetails() + ",but pcs not find ticket record.");
                        collectorMBox.send(new Msg(id, mbox, Msg.Type.IncorrectTicketId, msg.getDetails()));
                    }

                    break;

                case Terminate:
                    quit = true;
                    break;

                default:
                    appendTextArea(id + ": Receive inserted ticket id : " + msg.getDetails() + ",but pcs not find ticket record.", "Warning");
                    //log.warning(id + ": unknown message type: [" + msg + "]");
            }
        }

        // declaring our departure
        appKickstarter.unregThread(this);
        log.info(id + ": terminating...");
    } // run


    //------------------------------------------------------------
    // run
    private void handleTimesUp(Msg msg) {
        appendTextArea(id + ": Receive inserted ticket id : " + msg.getDetails() + ",but pcs not find ticket record.", "Info");
        //log.info("------------------------------------------------------------");
        switch (Timer.getTimesUpMsgTimerId(msg)) {
            case PollTimerID:
                appendTextArea("Poll: " + msg.getDetails(), "Info");
                //log.info("Poll: " + msg.getDetails());
                gateMBox.send(new Msg(id, mbox, Msg.Type.Poll, ""));
                Timer.setTimer(id, mbox, pollTime, PollTimerID);
                break;

            case OpenCloseGateTimerID:                    // for demo only!!!
                if (gateIsClosed) {
                    appendTextArea(id + ": Open the gate now (for demo only!!!)", "Info");
                    //log.info(id + ": Open the gate now (for demo only!!!)");
                    gateMBox.send(new Msg(id, mbox, Msg.Type.GateOpenRequest, ""));
                } else {
                    appendTextArea(id + ": Close the gate now (for demo only!!!)", "Info");
                    //log.info(id + ": Close the gate now (for demo only!!!)");
                    gateMBox.send(new Msg(id, mbox, Msg.Type.GateCloseRequest, ""));
                }
                Timer.setTimer(id, mbox, openCloseGateTime, OpenCloseGateTimerID);
                break;

            default:
                appendTextArea(id + ": why am I receiving a timeout with timer id " + Timer.getTimesUpMsgTimerId(msg), "Severe");
                //log.severe(id + ": why am I receiving a timeout with timer id " + Timer.getTimesUpMsgTimerId(msg));
                break;
        }
    } // handleTimesUp

    //------------------------------------------------------------
    // displayMessage
    protected void appendTextArea(String Message,String Type) {
        log.info(Message);
    } // displayMessage

} // PCSCore
