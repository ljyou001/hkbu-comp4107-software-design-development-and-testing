package PCS.Classes;

import PCS.PCSStarter;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class Ticket {
    //private int id;
    private LocalDateTime enteringTime;
    private LocalDateTime payTime;
    private double fee;
    private int payMachineld;
    private boolean isPay;

    public Ticket(){
        LocalDateTime now = LocalDateTime.now();
        this.enteringTime = now;
        this.payTime = null;
        this.fee = 0;
        this.payMachineld = 0;
        this.isPay = true;
    }

    public boolean isPay(){
        return isPay;
    }

    public boolean isExceedTime(int stayTime){
        LocalDateTime allowLevelTime = payTime.plusMinutes(stayTime);
        LocalDateTime now = LocalDateTime.now();
        return !now.isBefore(allowLevelTime);
    }

}
